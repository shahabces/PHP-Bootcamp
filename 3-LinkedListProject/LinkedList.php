<?php

class LinkedList
{
    public $firstNode;

    public function insert($newNode)
    {
        if (is_null($this->firstNode)) {
            $this->firstNode = $newNode;
            return true;
        }

        $tmp = $this->firstNode;

        while (!is_null($tmp->next)) {
            $tmp = $tmp->next;
        }

        $tmp->next = $newNode;

        return true;
    }

    public function print_node()
    {
        $tmp = $this->firstNode;
        while (!is_null($tmp)) {
            echo "$tmp->data<br>";
            $tmp = $tmp->next;
        }
    }

    public function search($data)
    {
        $tmp = $this->firstNode;

        while (!is_null($tmp)) {
            if ($tmp->data === $data) {
                return $tmp;
            }
            $tmp = $tmp->next;
        }

        return null;
    }

    public function delete($data)
    {
        $currentNode = $this->firstNode;
        $previousNode = null;

        while (!is_null($currentNode)) {
            if ($currentNode->data === $data) {
                if (is_null($previousNode)) {
                    $this->firstNode = $currentNode->next;
                } else {
                    $previousNode->next = $currentNode->next;
                }
                return true;
            }
            $previousNode = $currentNode;
            $currentNode = $currentNode->next;
        }

        return false;
    }
}

?>
