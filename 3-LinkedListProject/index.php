<?php

include "Node.php";
include "LinkedList.php";

$linkedList = new LinkedList();

$linkedList->insert(new Node(1));
$linkedList->insert(new Node(2));
$linkedList->insert(new Node(3));
$linkedList->insert(new Node(4));

$linkedList->print_node();


$sampleNode = $linkedList->search(3);
if (is_null($sampleNode)) {
    echo "not found...";
} else {
    echo "searched node founded and is : $sampleNode->data <br>";
}

$linkedList->delete(3);
$linkedList->print_node();

?>