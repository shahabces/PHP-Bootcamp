<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{

    use softDeletes;

    protected $table = "departments";
    protected $primaryKey = "id";


    protected $fillable = [
        'department_name', 'department_status', 'department_parent',
    ];

    function scopeActive ($query, $status){
        return $query->where('department_status', $status);
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

}
