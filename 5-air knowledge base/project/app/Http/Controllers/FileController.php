<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileController extends Controller
{
    function index()
    {
        $files = Storage::allFiles('public/files');
//        dd($files);
        return view('show_file', ['files' => $files]);
    }

    function create()
    {
        return view('upload_file');
    }

    function store(Request $request)
    {
//        Storage::disk('public')->putFileAs('images', $request->file('img'),
//            Str::random(5) . '.' . $request->file('img')->extension());

        $request->file->storeAs(
            'files',
            Str::random(5) . '.' . $request->file('file')->extension(),
            'public'
        );
        return redirect('files');
//        $request->file('img')->store('images');
    }

    public function destroy(Request $request)
    {

//        dd($request->path);
        Storage::delete($request->path);
        return redirect('files');
    }
}
