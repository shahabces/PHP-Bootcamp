<?php

namespace App\Http\Controllers;

use App\Department;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function getDepartmentForUser($id)
    {
        $department = User::findOrFail($id)->department;
        dd($department);
    }

    public function getSingleUser($id)
    {
        $results = User::findOrFail($id);
        dd($results->toJson(JSON_PRETTY_PRINT));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_users = User::all();
        dd($all_users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all_departments = Department::all();
        $roles = Role::all();

//        $all_departments = DB::table('departments')->whereNull('deleted_at')->get();
        return view('add_user', ['all_departments' => $all_departments,
            'all_roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tmp_request = $request->all();
        $tmp_request['password'] = Hash::make($request->password);

        $user = User::create($tmp_request);
        $user->roles()->attach($tmp_request['role_id']);

        return redirect("/users");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        dd($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $all_departments = Department::all();
        $dpt_name = Department::where('id', $user->getAttribute("department_id"))
            ->select('department_name')->firstOrFail();

        return view('edit_user', ['user' => $user,
            'all_departments' => $all_departments,
            'department_name' => $dpt_name]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return redirect("/users/" . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        redirect('/users');
    }
}
