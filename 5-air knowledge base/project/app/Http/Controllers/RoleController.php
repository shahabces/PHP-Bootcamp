<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        foreach ($roles as $role) {
        dump($role->users()->get());
    }

//        $users = User::all();
//
//        foreach ($users as $user) {
//            dump($user->roles()->get());
//        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $role1 = new Role();
        $role1['role_name'] = 'role1';
        $role1->save();

        $role2 = new Role();
        $role2['role_name'] = 'role2';
        $role2->save();


        $user1 = new User();
        $user1['first_name'] = 'name1';
        $user1['last_name'] = 'family1';
        $user1['email'] = 'email1';
        $user1['phone_number'] = 'phone1';
        $user1->save();

        $user2 = new User();
        $user2['first_name'] = 'name2';
        $user2['last_name'] = 'family2';
        $user2['email'] = 'email2';
        $user2['phone_number'] = 'phone2';
        $user2->save();

//        $role1 = Role::where('role_name', 'role1')->first();
//        $role2 = Role::where('role_name', 'role2')->first();
//        $user = User::where('first_name', 'name1')->first();

        $user1->roles()->attach($role1);
        $user1->roles()->attach($role2);

        $user2->roles()->attach($role2);

        return redirect('/roles');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {

    }
}
