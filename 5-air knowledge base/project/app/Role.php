<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use softDeletes;

    protected $table = "roles";
    protected $primaryKey = "id";


    protected $fillable = [
        'role_name',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

}
