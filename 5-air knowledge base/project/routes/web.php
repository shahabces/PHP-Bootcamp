<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/hello', function () {
//    return view('add_dpt');
//});

Route::resource('departments', 'DepartmentController')->middleware('auth');
Route::resource('groups', 'GroupController')->middleware('auth');;
Route::resource('users', 'UserController')->middleware('auth');;
Route::resource('roles', 'RoleController')->middleware('auth');;

//Route::get('/api/user/{id}', 'UserController@getSingleUser');

//Route::get('/department/{id}/users', 'DepartmentController@getUsersForDepartment');
//Route::get('/user/{id}/department', 'UserController@getDepartmentForUser');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/files', 'FileController@index');
Route::get('/files/create', 'FileController@create');
Route::post('/files', 'FileController@store');
Route::post('/files/delete', 'FileController@destroy');
