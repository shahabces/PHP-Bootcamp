<?php

/* @var $factory Factory */

use App\Department;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Department::class, function (Faker $faker) {
    $dptStatus = ['active', 'deactive'];
    return [
        'dpt_name' => $faker->name,
        'dpt_status' => $dptStatus[rand(0, 1)],
        'dpt_parent' => null,
    ];
});
