<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<form method="post" action="{{url('/departments/'.$department->getAttributeValue("id"))}}">

    @csrf
    @method('PUT')
    <label for="name">Department Name:</label>
    <input type="text" id="name" name="department_name" value="{{$department->getAttributeValue("department_name")}}"><br><br>

    <label>parent department:</label>
    <select name="department_parent">
        <option value="{{$department->getAttributeValue("id")}}">
            {{$department->getAttributeValue("department_name")}}
        </option>
        @foreach($all_departments as $dpt)
            @if($dpt->id === $department->getAttributeValue("id"))
                @continue;
            @endif
            <option value="{{$dpt->id}}">
                {{$dpt->department_name}}
            </option>
        @endforeach
    </select><br><br>

    <label for="dpt_status">Status:</label>
    @if($department->getAttributeValue("department_status") === "active")
        <input type="radio" name="department_status" value="active" checked>
        <label>active</label>
        <input type="radio" name="department_status" value="deactive">
        <label>deactive</label><br><br>
    @else
        <input type="radio" name="department_status" value="active">
        <label>active</label>
        <input type="radio" name="department_status" value="deactive" checked>
        <label>deactive</label><br><br>
    @endif

    <button type="submit">Submit</button>
</form>
</body>
</html>
