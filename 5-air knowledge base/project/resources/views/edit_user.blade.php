<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<form method="post" action="{{url('/users/'.$user->getAttribute("id"))}}">

    @csrf
    @method('PUT')
    <label>first_name:</label>
    <input type="text" name="first_name" value="{{$user->getAttribute("first_name")}}"><br><br>

    <label>first_name:</label>
    <input type="text" name="last_name" value="{{$user->getAttribute("last_name")}}"><br><br>

    <label>phone_number:</label>
    <input type="text" name="phone_number" value="{{$user->getAttribute("phone_number")}}"><br><br>
    <label>department:</label>
    <select name="department_id">
        <option value="{{$user->getAttribute('department_id')}}">
            {{$department_name->getAttribute('department_name')}}
        </option>
        @foreach($all_departments as $dpt)
            @if($dpt->id === $user->getAttribute("department_id"))
                @continue;
            @endif
            <option value="{{$dpt->id}}">
                {{$dpt->department_name}}
            </option>
        @endforeach
    </select><br><br>

    <label>role:</label>
    <select name="role">
        <option value="{{$user->getAttribute("role")}}">{{$user->getAttribute("role")}}</option>
        @foreach(['admin', 'master_engineer', 'team_member'] as $role)
            @if($user->getAttribute("role")!==$role)
                <option value="{{$role}}">{{$role}}</option>
            @endif
        @endforeach
    </select><br><br>

    <label>email:</label>
    <input type="text" name="email" value="{{$user->getAttribute("email")}}"><br><br>

    <button type="submit">Submit</button>

</form>

</body>
</html>
