<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>

<form method="post" action="{{url('/users')}}">

    @csrf
    <label>first_name:</label>
    <input type="text" name="first_name"><br><br>

    <label>first_name:</label>
    <input type="text" name="last_name"><br><br>

    <label>phone_number:</label>
    <input type="text" name="phone_number"><br><br>

    <label>department:</label>
    <select name="department_id">
        @foreach($all_departments as $dpt)
            <option value="{{$dpt->id}}">
                {{$dpt->department_name}}
            </option>
        @endforeach
    </select><br><br>

    <label>role:</label>
{{--    {{dd($all_roles)}}--}}
    <select name="role_id">


        @foreach($all_roles as $role)
            <option value="{{$role->id}}">{{$role->role_name}}</option>
        @endforeach
{{--        <option value="admin">admin</option>--}}
{{--        <option value="master_engineer">master_engineer</option>--}}
{{--        <option value="team_member">team_member</option>--}}
    </select><br><br>

    <label>email:</label>
    <input type="text" name="email"><br><br>

    <label>password:</label>
    <input type="text" name="password"><br><br>

    <button type="submit">Submit</button>

</form>

</body>
</html>
