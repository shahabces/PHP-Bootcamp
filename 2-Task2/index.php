<?php

require_once "autoload.php";

try {
    $arr = new ArrayWrapper([[1, 2]]);
    var_dump($arr->all());
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper([1, 2]);
    echo "<br>sum = " . $arr->sum();
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper([1, 2]);
    echo "<br>avg = " . $arr->avg();
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper([[1, 2], [3, 4, 2]]);
    echo "<br>count = " . $arr->count();
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper([3, 4, 2, 1, 0]);
    echo "<br> chuck";
    var_dump($arr->chunk(2));
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper([3, 4, 2, 1, 0]);
    $arr->concat([-1, -2, 3]);
    echo "<br> concat";
    var_dump($arr->all());
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper([3, 4, 2, 1, 0]);
    $arr->concat($arr);
    echo "<br> concat with own";
    var_dump($arr->all());
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper(["k1" => -1, "k2" => -2, "k3" => 3]);
    $val = $arr->searchByKey("k2");
    echo "<br> serch by key";
    if ($val === false) {
        echo "<br>not exist";
    } else {
        var_dump($val);
    }
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper(["k1" => -1, "k2" => -2, "k3" => 3]);
    $val = $arr->searchByValue(-2);
    echo "<br> serch by value";
    if ($val === false) {
        echo "<br>not exist";
    } else {
        var_dump($val);
    }
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arrayWrapper = new ArrayWrapper([1, 2, 3]);
    echo "<br>implode:<br>";
    echo $arrayWrapper->my_implode(",");
} catch (MyException $myException) {
    echo $myException->__toString();
}

echo "<br>===========================================";
try {
    $arrayWrapper = new ArrayWrapper([["camp_id" => 7475, "name" => "PHP/Laravel"],
        ["camp_id" => 648, "name" => "Front-end"]]);
    echo "<br>implode:<br>";
    echo $arrayWrapper->my_implode(",", "name");
} catch (MyException $myException) {
    echo $myException->__toString();
}
echo "<br>===========================================";

try {
    $arrayWrapper = new ArrayWrapper([1, 2, 3]);
    echo "<br>isEmpty:<br>";
    if ($arrayWrapper->isEmpty()) {
        echo "<br> empty";
    } else {
        echo "<br> not empty";
    }
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arrayWrapper = new ArrayWrapper([]);
    echo "<br>implode:<br>";
    $arrayWrapper->push(1);
    var_dump($arrayWrapper->all());
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arrayWrapper = new ArrayWrapper([1, 2, 3]);
    echo "<br>implode:<br>";
    $arrayWrapper->pop();
    var_dump($arrayWrapper->all());
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arrayWrapper = new ArrayWrapper([8, 5, 333, 1]);
    $arrayWrapper->sort();
    var_dump($arrayWrapper->all());
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arrayWrapper = new ArrayWrapper([8, 5, 333, 1]);
    $arrayWrapper->sort("desc");
//    var_dump($arrayWrapper->all());
    print_r($arrayWrapper->all());
} catch (MyException $myException) {
    echo $myException->__toString();
}

try {
    $arr = new ArrayWrapper(["a" => 1, 3, 5]);
    echo $arr->toJson();
} catch (MyException $myException) {
    echo $myException->__toString();
}
