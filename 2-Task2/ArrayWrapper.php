<?php


class ArrayWrapper
{
    private $arr = [];

    public function __construct($arr)
    {
        if (is_array($arr))
            $this->arr = $arr;
        else
            throw MyException::notNDimArray();
    }

    public function all()
    {
        return $this->arr;
    }

     

    public function sum()
    {
        return array_sum($this->arr);
    }

    public function avg()
    {
        return array_sum($this->arr) / count($this->arr);
    }

    public function count()
    {
        return count($this->arr);
    }

    public function chunk($num)
    {
        return array_chunk($this->arr, $num);
    }

    public function concat($arr2)
    {
        if (is_array($arr2)) {
            $this->arr = array_merge($this->arr, $arr2);
        } else if (get_class($arr2) === get_class($this)) {
            $this->arr = array_merge($this->arr, $arr2->all());
        } else {
            throw MyException::invalidInputType("must be array or ArrayWrapper");
        }
    }

    public function searchByKey($key)
    {
        return isset($this->arr[$key]) ? $this->arr[$key] : false;
    }

    public function searchByValue($val)
    {
        $idx = array_search($val, $this->arr);
        if ($idx === -1) {
            return false;
        }
        return [$idx => $this->arr[$idx]];
    }

    public function my_implode($glue, $sampleKey = null)
    {
        if (is_null($sampleKey)) {
            return implode($glue, $this->arr);
        } else {
            $tmpArr = [];
            array_walk_recursive($this->arr, function ($item, $key) use (&$tmpArr, $sampleKey) {
                if ($key === $sampleKey) {
                    array_push($tmpArr, $item);
                }
            });
//            var_dump($this->tmpArr);
            return implode($glue, $tmpArr);
        }
    }

    public function isEmpty()
    {
        if (is_null($this->arr) || count($this->arr) === 0) {
            return true;
        }
        return false;
    }

    public function merge($arr2)
    {
        $this->arr = array_merge($this->arr, $arr2);
    }

    public function push($val)
    {
        array_push($this->arr, $val);
    }

    public function pop()
    {
        return array_pop($this->arr);
    }

    public function sort($kind = "asc")
    {
        if ($kind === "desc") {
            rsort($this->arr);
        } else {
            sort($this->arr);
        }
    }

    public function toJson()
    {
        return json_encode($this->arr);
    }

}