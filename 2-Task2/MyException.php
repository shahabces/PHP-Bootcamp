<?php


class MyException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function notNDimArray()
    {
        return new MyException("not a array", 1);

    }

    public static function canNotLoadClass($msg)
    {
        return new MyException($msg, 2);
    }

    public static function invalidInputType($msg)
    {
        return new MyException($msg, 3);
    }

    public function __toString()
    {
        return "<br>Exception occured:<br> code: {$this->code} <br> message: {$this->message}<br>";
    }
}