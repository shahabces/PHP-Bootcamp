<?php

use app\exceptions\ClassNotFoundException;

spl_autoload_register(function ($className) {
    if (file_exists($className . ".php")) {
        require_once $className . ".php";
    } else {
        throw new ClassNotFoundException($className . ".php not exist!");
    }
});

?>