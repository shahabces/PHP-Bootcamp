<?php

use app\classes\Bootcamp;
use app\classes\User;
use app\exceptions\ClassNotFoundException;
use app\framework\Database;

require_once("autoload.php");

try {
    $user = new User();
    $bootcamp = new Bootcamp();
    $db = new Database();
    $obj = new SampleClass();
} catch (ClassNotFoundException $classNotFoundException) {
    echo $classNotFoundException->getMessage();
}